from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
import string
import requests
import os
import time
import random

from multiprocessing import Pool
from multiprocessing import current_process


def write_b(file):
    print(current_process().name)
    file_name = file.split('/')[-1]
    file_dir = r'/home/v-osipov/PycharmProjects/vba/data/new_sc/'
    time.sleep(0.2)

    if not os.path.isfile(file_dir + file_name):
        try:
            query = requests.get(file, timeout=4)
            if file_name:
                data = query.content
                with open(file_dir + file_name, 'wb') as w:
                    w.write(data)
                    print(file, 'writed')
            else:
                print('not doc')
        except Exception:
            print('плохо')
    else:
        print('file exist')


def seach(query):
    fp = webdriver.FirefoxProfile(r'/home/v-osipov/.mozilla/firefox/89dlwo41.default-release')
    option=Options()
    option.headless=True
    driver = webdriver.Firefox(fp,options=option)

    driver.get('https://google.com')
    driver.implicitly_wait(2)
    seach = driver.find_elements_by_name("q")[0]
    seach.send_keys(query)
    seach.send_keys(Keys.ENTER)

    return driver


def parse(page):
    parser = BeautifulSoup(page, features="html.parser")
    divs = parser.find_all(class_='r')
    return [href.a.attrs['href'] for href in divs]


def next_page(driver):
    try:
        next_ = driver.find_elements_by_id("pnnext")[0]
        next_.click()
        return True
    except IndexError:
        try:
            driver.implicitly_wait(2)
            next_ = driver.find_elements_by_id("captcha-form")[0]
            print("Капча")
            time.sleep(60 * 2)
            return False
        except Exception:
            return False


exts = ['doc', 'xlsm', 'xlsb']

for n in range(1,10):
    for i in random.sample(string.ascii_lowercase,n):
        for f in random.sample(string.ascii_lowercase,n):
            for ext in random.sample(exts, len(exts)):
                q = seach(f'{i}{f} filetype:{ext}')
                while True:
                    try:
                        with Pool(5) as p:
                            time.sleep(3)
                            p.map(write_b, parse(q.page_source))
                        if not next_page(q):
                            q.close()
                            break
                    except Exception as e:
                        print(e)
                        break

