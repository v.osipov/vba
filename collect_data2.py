import pandas as pd
from oletools.olevba import VBA_Parser, FileOpenError
import os
import random
from multiprocessing import Pool
from datetime import datetime
import numpy as np



class Feature_Extractor:
    def __init__(self,file=None,path_benign=r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts/',
                 path_malicios=r'/home/v-osipov/PycharmProjects/vba/data/malicious/'):

        self.file=file
        self.path_benign=path_benign
        self.path_malicios=path_malicios
        self.files=[]

    def extract_feature_from_paths(self,path_benign=r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts/'
                                   ,path_malicios=r'/home/v-osipov/PycharmProjects/vba/data/malicious/'):
        self.path_benign = path_benign
        self.path_malicios = path_malicios

    def __from_path_to_files__(self):
        files_benign = list(map(lambda x: self.path_benign + x, os.listdir(self.path_benign)))

        files_malicious = list(
            map(lambda x: self.path_malicios + x,
                random.sample(os.listdir(self.path_malicios), len(self.files_benign))))

        self.files=files_benign+files_malicious

    def text_extract(self,file):
        k = 1 if file.find('malicious') != -1 else 0
        try:
            with open(file) as f:
                x = [f.read(), k]
                return x
        except UnicodeDecodeError:
            return [np.nan] * 2

    def from_oletools(self,file):
        k = 1 if file.find('malicious') != -1 else 0
        try:
            vbaparser = VBA_Parser(file)
            results = vbaparser.analyze_macros()
            buff = ''
            if not results:
                buff = 'no_results'
            else:
                for kw_type, keyword, description in results:
                    buff += f"kw_type={kw_type}  keyword={keyword}   description={description}\n"

            x = [vbaparser.nb_autoexec, vbaparser.nb_suspicious, vbaparser.nb_iocs,
                 vbaparser.nb_hexstrings, vbaparser.nb_base64strings,
                 vbaparser.nb_dridexstrings, vbaparser.nb_vbastrings, buff, k]
            vbaparser.close()

            return x
        except FileOpenError:
            return [np.nan] * 9

    def panda_make_sets(self,array):

        columns = ['AutoExec keywords', 'Suspicious keywords', 'IOCs',
                   "Hex obfuscated strings", "Base64 obfuscated strings",
                   "Dridex obfuscated strings", "VBA obfuscated strings", "text", "virus"]

        choose = {9: [columns, 'olesets'],
                  2: [columns[-2:], 'textsets']}

        panda = pd.DataFrame(array, columns=choose[len(array[0])][0])
        panda.dropna(inplace=True)

        if choose[len(array[0])][1] == "olesets":
            panda_text = panda[columns[-2:]]
            panda.drop(columns=columns[-2], inplace=True)
            panda.to_csv('numeric_dataset.csv')
            panda_text.to_csv('text_dataset_ole.csv')
        else:
            panda.to_csv('text_dataset.csv')




def from_path_to_files(path_benign=r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts/'
                       , path_malicios=r'/home/v-osipov/PycharmProjects/vba/data/malicious/'):

    files_benign = list(map(lambda x: path_benign + x, os.listdir(path_benign)))

    files_malicious = list(
        map(lambda x: path_malicios + x,
            random.sample(os.listdir(path_malicios), len(files_benign))))

    return files_benign+files_malicious



def text_extract(file):
    k = 1 if file.find('malicious') !=-1 else 0
    try:
        with open(file) as f:
            x=[f.read(),k]
            return x
    except UnicodeDecodeError:
        return [np.nan]*2


def from_oletools(file):
    k = 1 if file.find('malicious') !=-1 else 0
    try:
        vbaparser = VBA_Parser(file)
        results = vbaparser.analyze_macros()
        buff=''
        if not results:
            buff='no_results'
        else:
            for kw_type, keyword, description in results:
                buff +=f"kw_type={kw_type}  keyword={keyword}   description={description}\n"

        x = [vbaparser.nb_autoexec, vbaparser.nb_suspicious, vbaparser.nb_iocs,
             vbaparser.nb_hexstrings, vbaparser.nb_base64strings,
             vbaparser.nb_dridexstrings, vbaparser.nb_vbastrings,buff, k]
        vbaparser.close()

        return x
    except FileOpenError:
        return [np.nan]*9




def panda_make_sets(array):

    columns=['AutoExec keywords', 'Suspicious keywords', 'IOCs',
                   "Hex obfuscated strings", "Base64 obfuscated strings",
                   "Dridex obfuscated strings", "VBA obfuscated strings","text", "virus"]

    choose = {9: [columns,'olesets'],
              2:[columns[-2:],'textsets']}



    panda = pd.DataFrame(array, columns=choose[len(array[0])][0])
    panda.dropna(inplace=True)

    if choose[len(array[0])][1] == "olesets":
        panda_text=panda[columns[-2:]]
        panda.drop(columns=columns[-2],inplace=True)
        panda.to_csv('numeric_dataset.csv')
        panda_text.to_csv('text_dataset_ole.csv')
    else:
        panda.to_csv('text_dataset.csv')




if __name__ == '__main__':
    data = from_path_to_files()
    start=datetime.now()

    with Pool(8) as p:
        # text_list=p.map(text_extract,data)
        ole_list=p.map(from_oletools,data)



    # panda_make_sets(text_list)
    panda_make_sets(ole_list)

    print(datetime.now() - start)

