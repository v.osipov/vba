from oletools.olevba import VBA_Parser, TYPE_OLE, TYPE_OpenXML, TYPE_Word2003_XML, TYPE_MHTML, FileOpenError
import os
import random
import pandas as pd

path_benign = r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts/'
path_malicios = r'/home/v-osipov/PycharmProjects/vba/data/malicious/'

files_benign = list(map(lambda x: path_benign + x, os.listdir(path_benign)))

files_malicious = list(
    map(lambda x: path_malicios + x, random.sample(os.listdir(path_malicios)[:len(files_benign)], len(files_benign))))


#


def test(file):
    print(file)
    vbaparser = VBA_Parser(file)
    results = vbaparser.analyze_macros()
    if results:
        for kw_type, keyword, description in results:
            print('type=%s - keyword=%s - description=%s' % (kw_type, keyword, description))
        print('_' * 39)
        print('AutoExec keywords:', vbaparser.nb_autoexec)
        print('Suspicious keywords:', vbaparser.nb_suspicious)
        print('IOCs: %d', vbaparser.nb_iocs)
        print('Hex obfuscated strings: ', vbaparser.nb_hexstrings)
        print('Base64 obfuscated strings:', vbaparser.nb_base64strings)
        print('Dridex obfuscated strings:', vbaparser.nb_dridexstrings)
        print('VBA obfuscated strings: ', vbaparser.nb_vbastrings)
    else:
        print('Not found')


def create_pd_sample(files,m):
    temp_data = pd.DataFrame()
    for file in files:
        try:
            vbaparser = VBA_Parser(file)
            vbaparser.analyze_macros()
            temp_data = temp_data.append({'file_name': file, 'AutoExec keywords': vbaparser.nb_autoexec
                                             , 'Suspicious keywords': vbaparser.nb_suspicious,
                                          'IOCs': vbaparser.nb_iocs,
                                          "Hex obfuscated strings": vbaparser.nb_hexstrings,
                                          "Base64 obfuscated strings": vbaparser.nb_base64strings,
                                          "Dridex obfuscated strings": vbaparser.nb_dridexstrings,
                                          "VBA obfuscated strings": vbaparser.nb_vbastrings,
                                          'virus':m}, ignore_index=True)
            vbaparser.close()
        except FileOpenError:
            pass
    return temp_data




benign = create_pd_sample(files_benign,0)
malicious = create_pd_sample(files_malicious,1)

data3 = benign.append(malicious, ignore_index=True)

data3.to_csv('data3.csv')
