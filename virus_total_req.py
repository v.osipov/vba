import requests
import time

api_key = '59249f0b8146ba22d3d2fe117ac4b83a2743df39a25afa1a7df82239476f984d'



def send_scan(file):
    url = 'https://www.virustotal.com/vtapi/v2/file/scan'

    params = {'apikey': api_key}

    response = requests.post(url, files=file, params=params)

    return response.json()


def get_info(response):
    url = 'https://www.virustotal.com/vtapi/v2/file/report'

    params = {'apikey': api_key, 'resource': response['resource']}

    response = requests.get(url, params=params)


    while response.json()['verbose_msg']!='Scan finished, information embedded':
        time.sleep(180)
        response = requests.get(url, params=params)

    return response.json()


def verdict(info):
    print(info)
    if info['positives'] > 0:
        print("malicious")
        return True
    else:
        print("benign")
        return False


def verdict_file(path):
    return verdict(get_info(send_scan({'file': (path, open(path, 'rb'))})))













