import oletools
from oletools.olevba import VBA_Parser, TYPE_OLE, TYPE_OpenXML, TYPE_Word2003_XML, TYPE_MHTML
import os
path='new_sc/'
files= list(map(lambda x :path+x, os.listdir(path)))

macros=0

for file in files:
    try:
        if VBA_Parser(file).detect_vba_macros():
            macros+=1
            print('find macros')
        else:
            os.remove(file)
    except oletools.olevba.FileOpenError:
        os.remove(file)


print(len(files),macros)