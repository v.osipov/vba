import os
import re

path = r'/home/v-osipov/PycharmProjects/vba/data/malicious/'
files = os.listdir(path)
print(len(files))

for file in files:
    with open(path + file,'r') as f:
        strings=f.readlines()
        if len(strings)<=3:
            os.remove(path + file)
            continue
        if len(re.findall(r'malicious', strings[0])) > 0:
            strings=strings[1:]
        else:
            continue
    with open(path + file,'w') as f:
        f.writelines(strings)
        print('file changed!',file)


