import os
import re

path = r'/home/v-osipov/PycharmProjects/vba/data/benign/'
files = os.listdir(path)
print(len(files))

remove = False

for file in files:
    count_lines = 0
    with open(path + file) as f:
        for line in f:
            count_lines += 1
            if len(re.findall(r'(\<(/?[^>]+)>)', line)) > 0:
                os.remove(path + file)
                remove = True
                print("Rem!")
                break
            if count_lines > 40:
                break

    if count_lines < 14 and remove is False:
        os.remove(path + file)
        remove = False
