import pandas as pd
from oletools.olevba import VBA_Parser, FileOpenError
import os
import random
from multiprocessing import Pool
import numpy as np


class Feature_Extractor:

    def __init__(self, virus, file=None, path_from_extract=None):

        self.file = file
        self.files = None
        self.panda = pd.DataFrame()
        self.virus = virus
        self.source_dataset = pd.DataFrame()
        self.path_from_extract = path_from_extract
        self.source_path_VBA = None

    def extract_file(self, file=None):

        source = self.__source_extract__(self.file or file)
        oletool = self.__from_oletools__(self.file or file)
        self.source_dataset = self.__panda_make_source_dataset__([source])
        self.__panda_make__([oletool])
        return self.source_dataset

    def set_path_from_extract(self, path):
        self.path_from_extract = path
        self.files = [self.path_from_extract + file for file in os.listdir(self.path_from_extract)]

    def set_source_path_to_extract_VBA(self, path):
        self.source_path_VBA = path

    def get_oletools_descr(self):
        if not self.panda.empty:
            return self.panda[['text', 'virus']]
        else:
            with Pool(8) as p:
                self.__panda_make__(p.map(self.__from_oletools__, self.files))
                return self.panda[['text', 'virus']]

    def get_oletools_stat(self):
        if not self.panda.empty:
            return self.panda[[x for x in self.panda.columns if x != 'text']]
        else:
            with Pool(8) as p:
                self.__panda_make__(p.map(self.__from_oletools__, self.files))
                return self.panda[[x for x in self.panda.columns if x != 'text']]

    def get_source_VBA_in_pandas(self):
        with Pool(8) as p:
            return self.__panda_make_source_dataset__(p.map(self.__source_extract__, self.files))

    def get_extracted_source_VBA(self):
        dirs = [self.source_path_VBA + file + '/' for file in os.listdir(self.source_path_VBA)]
        files = []
        for dir in dirs:
            files.extend(map(lambda x: dir + x, os.listdir(dir)))
        with Pool(8) as p:
             self.source_dataset = self.__panda_make_source_dataset__(p.map(self.__source_extract__, files))
             return self.source_dataset


    def __source_extract__(self, file):

        k = 1 if self.virus else 0
        try:
            with open(file,'r') as f:
                x = [f.read(), k]
                return x
        except UnicodeDecodeError:
            return [np.nan] * 2

    def __from_oletools__(self, file):
        k = 1 if self.virus else 0
        try:
            vbaparser = VBA_Parser(file)
            results = vbaparser.analyze_macros()
            macros = vbaparser.extract_all_macros()
            if macros:
                struct = {"path_file": None}
                for macro in macros:
                    struct.update({macro[2]: macro[3], "path_file": macro[0]})

                scripts = list(struct.keys())[1:]

                if scripts[0].count('/') < 1:
                    file_sc_dir = self.source_path_VBA + file.split('/')[-1][:-4] + '/'
                    try:
                        os.mkdir(file_sc_dir)
                        for name_sc in scripts:
                            with open(file_sc_dir + '/' + name_sc, 'w') as w:
                                w.write(struct[name_sc])

                    except FileExistsError:
                        print('file exist')

            buff = ''
            if not results:
                buff = 'no_results'
            else:

                for kw_type, keyword, description in results:
                    buff += f"kw_type={kw_type}  keyword={keyword}   description={description}\n"

            x = [vbaparser.nb_autoexec, vbaparser.nb_suspicious, vbaparser.nb_iocs,
                 vbaparser.nb_hexstrings, vbaparser.nb_base64strings,
                 vbaparser.nb_dridexstrings, vbaparser.nb_vbastrings, buff, k]
            vbaparser.close()

            return x
        except FileOpenError:
            return [np.nan] * 9
        except TypeError:
            return [np.nan] * 9

    def __panda_make__(self, array):

        columns = ['AutoExec keywords', 'Suspicious keywords', 'IOCs',
                   "Hex obfuscated strings", "Base64 obfuscated strings",
                   "Dridex obfuscated strings", "VBA obfuscated strings", "text", "virus"]

        self.panda = pd.DataFrame(array, columns=columns)
        self.panda.dropna(inplace=True)

    def __panda_make_source_dataset__(self, array):

        return pd.DataFrame(array, columns=['text', 'virus'])

    def pandas_to_csv(self):

        tables = {'source_VBA_dataset': self.source_dataset,
                  "ole_descrption": self.panda[['text', 'virus']],
                  "ole_num": self.panda[[x for x in self.panda.columns if x != 'text']]}

        for name, table in tables.items():
            if not table.empty:
                table.to_csv(f'{name}.csv')


test = Feature_Extractor(virus=False)
print(test.extract_file(r'/home/v-osipov/PycharmProjects/vba/data/new_sc/1.doc'))
print(test.get_oletools_stat())
print()

# test.set_path_from_extract(r'/home/v-osipov/PycharmProjects/vba/data/new_ben/')
# print(test.files)
# test.set_source_path_to_extract_VBA(r'/home/v-osipov/PycharmProjects/vba/data/source_vba/')
# print(test.get_oletools_stat())
# print(test.get_oletools_descr())
# print(test.get_extracted_source_VBA())
# test.pandas_to_csv()
#
