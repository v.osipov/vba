from torpy.http.requests import TorRequests
import json
import os
import shutil
from  multiprocessing import Pool
import sys, time
from daemon_class import daemon




class VirusTotal:
    def __init__(self):
        self.headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
            "Accept-Encoding": "gzip, deflate,",
            "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"}

        self.url = "https://www.virustotal.com/ui/files/upload_url"

    def get_verdict_from_vt(self,file):

        try:
            with TorRequests() as tor_requests:
                with tor_requests.get_session() as sess:
                    url = json.loads(sess.get(self.url, headers=self.headers).text)
                    with open(file, 'rb') as f:
                        postr = sess.post(url['data'], headers=self.headers, files={'file': f.read()})
                    print(postr.text)
                    analis = json.loads(postr.text)['data']['id']

                    url_analisys = f"https://www.virustotal.com/ui/analyses/{analis}"

                    response = sess.get(url_analisys, headers=self.headers)

                    while response.json()['data']['attributes']['status'] != 'completed':
                        time.sleep(10)
                        response = sess.get(url_analisys, headers=self.headers)

                    malicious = response.json()['data']['attributes']['stats']['malicious']

                    return malicious
        except Exception:
            self.get_verdict_from_vt(file)



class MyDaemon(daemon):
    def run(self):

        path = r'/home/v-osipov/PycharmProjects/vba/data/malicious/'
        files = [path + file for file in os.listdir(path)]
        with Pool(40) as p:
            p.map(move_file,files)
        print(time.time()-start)





def move_file(file):

    if check_file.get_verdict_from_vt(file)<1:
        try:
            shutil.move(file, r'/home/v-osipov/PycharmProjects/vba/data/benign_from_mal/')
            print('moved ', file, 'to benign_from_mal')
        except Exception as e:
            print(e)
            pass
    else:
        try:
            shutil.move(file, r'/home/v-osipov/PycharmProjects/vba/data/mal_from_mal/')
            print('moved ', file, 'to mal_from_mal')
        except Exception as e:
            print(e)
            pass




# if __name__=='__main__':
#     check_file = VirusTotal()
#     path = r'/home/v-osipov/PycharmProjects/vba/data/malicious/'
#     files = [path + file for file in os.listdir(path)]
#
#     with Pool(20) as p:
#         p.map(move_file,files)




if __name__ == "__main__":
    check_file = VirusTotal()
    start = time.time()
    daemon = MyDaemon('/tmp/antivirus.pid')

    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print
            ("Unknown command")
            sys.exit(2)
        sys.exit(0)
    else:
        print
        ("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)






