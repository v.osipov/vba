from multiprocessing import Pool
from multiprocessing import current_process
from bs4 import BeautifulSoup
import re
import requests
import os
import time
import shutil
import zipfile
from torpy.http.requests import TorRequests


path_sc = '/home/v-osipov/PycharmProjects/vba/data/arhs/'


def get_repo(page_html):
    soup = BeautifulSoup(page_html, 'html.parser')
    divs = soup.find_all(class_='f4 text-normal')
    buff = []
    for element in divs:
        string = element.a.attrs['data-hydro-click']
        url = re.search(r'(https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?', string)
        repo_url = url.group(0)
        buff.append(repo_url)
    return buff


def download_repo(url):
    time.sleep(2)
    from urllib.error import URLError
    try:
        req=requests.get(url + "/archive/master.zip")
    except URLError:
        pass
    while req.status_code!=200:
        print('need wait')
        print(req.headers)
        # req = requests.get(url + "/archive/master.zip")
        with TorRequests() as tor_requests:
            with tor_requests.get_session() as sess:
                    req = sess.get(url + "/archive/master.zip")

    zip_file=req.content

    try:
        file_name = "".join(url.split('/')[-2:]) + ".zip"
        with open(path_sc + file_name, 'wb') as writer:
            writer.write(zip_file)

        return extract_repo(path_sc + file_name)
    except EOFError:
        print('не удалось загрузить')
        pass
    except zipfile.BadZipFile:
        print('не зип')
    except FileExistsError:
        return extract_repo(path_sc + file_name)


def extract_repo(archive):
    print(current_process().name)
    pref_='/'+ archive.split('/')[-1][:-4]+'/'
    file = zipfile.ZipFile(archive, 'r')
    try:
        os.mkdir(path_sc+pref_)
    except FileExistsError:
        pass
    file.extractall(path_sc+pref_)
    files = [path_sc + pref_[1:]+  name.filename for name in file.filelist]
    file.close()
    os.remove(archive)
    root = path_sc+pref_[1:]
    print(root)
    for file_link in [file_link for file_link in files if file_link[-4:] in ['.cls', '.vba', '.bas']]:
        try:
            shutil.move(file_link, r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts')
            print('moved ',file_link,'to new_scrpts')
        except Exception as e:
            print(e)
            pass
    shutil.rmtree(root)


#
if __name__ == "__main__":
    job = []
    with Pool(9) as process:
        for n in range(1, 101):
            req = requests.get(f'https://github.com/search?l=Visual+Basic+.NET&p={n}&q=vba&type=Repositories')
            # req = requests.get(f"https://github.com/search?l=VBA&p={n}&q=vba&type=Repositories")
            if req.status_code==200:
                job.extend(get_repo(req.text))
                print(req.status_code)
            else:
                while req.status_code!=200:
                    if job:
                        print('page=',n,'\n')
                        if job:
                            try:
                                process.map(download_repo,job)
                            except Exception as e:
                                print(e)

                            job.clear()
                        print('i done')
                    else:
                        time.sleep(20)
                        req = requests.get(f'https://github.com/search?l=Visual+Basic+.NET&p={n}&q=vba&type=Repositories')
                        # req = requests.get(
                        #     f"https://github.com/search?l=VBA&p={n}&q=vba&type=Repositories")
                        print('i wait')




    #

