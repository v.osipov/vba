import os
import pandas as pd
import random


path_malicios = r'/home/v-osipov/PycharmProjects/vba/data/malicious/'

# files_benign = list(map(lambda x: path_benign + x, os.listdir(path_benign)))

files_malicious = list(
    map(lambda x: path_malicios + x, random.sample(os.listdir(path_malicios),1000 )))
#
data = pd.DataFrame()

for b, m in zip(files_benign, files_malicious):
    try:
        with open(m) as f:
            data = data.append({'file_name': m, 'text': f.read(), 'virus': 1}, ignore_index=True)
    except UnicodeDecodeError:
        pass

data.to_csv('data2.csv')
