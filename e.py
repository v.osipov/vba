from bs4 import BeautifulSoup as bs
import requests
import json
import os
import time
import string

# All files from google will be benign

headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
            "Accept-Encoding": "gzip, deflate,",
            "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (X11; Linux i686; rv:5.0) Gecko/20120819 Firefox/36.0"}

# https://www.google.com/search?q=filetype:xlsb&newwindow=1&sxsrf=ALeKk01HRjW2AdO5kEW9d4ndQKoOxGizTg:1594798505679&ei=qbEOX4_aKOGnrgTpu7fAAg&start=80&sa=N&ved=2ahUKEwjP0qyz387qAhXhk4sKHendDSg4RhDy0wN6BAgLEDo&biw=1848&bih=942
# link='https://www.google.com/search?q=filetype:xlsb&newwindow=1&sxsrf=ALeKf01bL2nOowycwu2FXggEP2HudbosVTg:1594798890120&ei=KrMOX6fyBsPJrgS4sJioAg&start=90&sa=N&ved=2ahUKEwinr9Xq4M7qAhXDpIsKHTgYBiU4UBDy0wN6BAgLEDo&biw=1848&bih=942'

results = []
# Get your string from your google search
# link = "https://www.google.com/search?q={}filetype:docm&newwindow=1&sxsrf=ACYBGNQryewtyEL9OMrCRU_ekXeBuDXy_Q:1578131725873&ei=DWEQXrb6NMT16QSVgLeQCg&start={}0&sa=N&ved=2ahUKEwj2tLfn1unmAhXEepoKHRXADaIQ8tMDegQICxAw&biw=1920&bih=981"
link="https://www.google.com/search?q={}+filetype:docm&newwindow=1&sxsrf=ACYBGNQryewtyEL9OMrCRU_ekXeBuDXy_Q:1578131725873&ei=DWEQXrb6T16QSVgLeQCg&start={}0&sa=N&ved=2ahUKEwj2tLfn1unmAhXEepoKHRXADaIQ8tMDegQICxAw&biw=1920&bih=981"
for i in string.ascii_lowercase:
    for f in string.ascii_lowercase:
        letrs = i
        for page in range(10):
            time.sleep(1.2)
            content = requests.get(link.format(letrs,page),headers=headers).text
            print(letrs, page)
            bsoup = bs(content, features="html.parser")
            # hrefs = bsoup.find_all("a", attrs={"target": "_blank", })
            divs = bsoup.find_all(class_='r')
            hrefs = [href.a.attrs['href'] for href in divs]

            print(f"Len of found hrefs: {len(hrefs)}")
            for href in hrefs:
                # href = href['href'][7:].split('&')[0]
                print(href)
                try:
                    if not os.path.isfile("new_benign/" + str(href.split('/')[-1])):

                        possible_pdf = requests.get(href, timeout=60)
                        headers = possible_pdf.headers
                        print(headers['Content-Type'])
                        cont_type=['application/vnd.ms-word.template.macroEnabled.12','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/msword'
                                   ,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",'application/vnd.ms-excel.sheet.macroEnabled.12',
                                   'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel',
                                   'application/vnd.ms-excel.sheet.binary.macroEnabled.12',"application/octet-stream"]
                        if headers['Content-Type'] in cont_type:

                            with open("new_benign/" + str(href.split('/')[-1]), 'wb') as f:
                                f.write(possible_pdf.content)

                            print("Downloading finished")

                        else:
                            print("it's not a pdf!")
                    else:
                        print("File already exist")

                except Exception as e:
                    print(e)