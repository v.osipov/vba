import pandas as pd
from oletools.olevba import VBA_Parser, FileOpenError
import os
import random


def from_path_to_files(path_benign=r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts/'
                       , path_malicios=r'/home/v-osipov/PycharmProjects/vba/data/malicious/'):
    files_benign = list(map(lambda x: [path_benign + x, 0], os.listdir(path_benign)))

    files_malicious = list(
        map(lambda x: [path_malicios + x, 1],
            random.sample(os.listdir(path_malicios)[:len(files_benign)], len(files_benign))))

    return files_benign + files_malicious


def for_bag_word(path, k=None):
    data = pd.DataFrame()
    files = path if isinstance(path, list) else [path, k]

    for path, k in files:
        try:
            with open(path) as f:
                data = data.append({'text': str(f.read()), 'virus': k}, ignore_index=True)
        except UnicodeDecodeError:
            pass

    data.to_csv("for_oletools_num.csv")
    return data


def for_oletools_num(path, k=None):
    temp_data = pd.DataFrame()
    files = path if isinstance(path, list) else [path, k]
    for path, k in files:
        try:
            vbaparser = VBA_Parser(path)
            vbaparser.analyze_macros()
            temp_data = temp_data.append({'AutoExec keywords': vbaparser.nb_autoexec
                                             , 'Suspicious keywords': vbaparser.nb_suspicious,
                                          'IOCs': vbaparser.nb_iocs,
                                          "Hex obfuscated strings": vbaparser.nb_hexstrings,
                                          "Base64 obfuscated strings": vbaparser.nb_base64strings,
                                          "Dridex obfuscated strings": vbaparser.nb_dridexstrings,
                                          "VBA obfuscated strings": vbaparser.nb_vbastrings,
                                          'virus': k}, ignore_index=True)
            vbaparser.close()
        except FileOpenError:
            pass

    temp_data.to_csv("for_oletools_num.csv")
    return temp_data


def for_oletools_txt(path, k=None):
    temp_data = pd.DataFrame()
    files = path if isinstance(path, list) else [path, k]
    for path, k in files:
        try:
            buff = ""
            results = VBA_Parser(path).analyze_macros()
            for kw_type, keyword, description in results:
                buff += description + '\n'
            temp_data = temp_data.append({"text": buff, 'virus': k}, ignore_index=True)

        except FileOpenError:
            pass

    temp_data.to_csv("for_oletools_txt.csv")

    return temp_data
