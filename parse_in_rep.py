from bs4 import BeautifulSoup
import re
import requests
import os


path_sc = os.getcwd()


def write_file(url, url_buffer=[],file_buffer=[],exs=['.vba','.cls','.bas']):

    soup = BeautifulSoup(requests.get(url).text, 'html.parser')
    dirs = soup.find_all(class_='css-truncate css-truncate-target d-block width-fit')
    if dirs:
        url_buffer=get_refs(dirs)+ url_buffer
        return write_file(url_buffer.pop(0), url_buffer,file_buffer)
    else:
        if url_buffer:
            if url[-4:] in exs:
                file_buffer.append(url)
            return write_file(url_buffer.pop(0), url_buffer,file_buffer)
        else:
            return file_buffer




def get_refs(obj):
    if obj:
        h_refs=[r'https://github.com'+
            href.a.attrs['href'] for href in obj if href.a.attrs['href'].find('/commit/')==-1]
        return h_refs
    else:
        return False


def get_page():
    for n in range(1,100):
        req=requests.get(f'https://github.com/search?l=Visual+Basic+.NET&p={n}&q=vba&type=Repositories')
        get_write_code(req.text)



#
#
def get_write_code(page_html):
    soup = BeautifulSoup(page_html, 'html.parser')
    divs = soup.find_all(class_='f4 text-normal')



    for element in divs:
        string = element.a.attrs['data-hydro-click']
        url = re.search(r'(https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?', string)
        url = url.group(0)
        repo_url=url
        print(write_file(repo_url))






            # soup=BeautifulSoup(requests.get(file_url).text,'html.parser')
            # files=soup.find_all(class_='css-truncate css-truncate-target d-block width-fit')
            # for file in files:
            #      if file.a.attrs['href'][-4:] in exs:
            #          print(file.a.attrs['href'])





        url = url.split('https://github.com/')[1]
        url = 'https://raw.githubusercontent.com/' + ''.join(url.split('/blob'))

        req=requests.get(url)

        file_name = url.split('/')[-1]
        try:
            with open(path_sc + file_name, 'w') as f:
                f.write(req.text)
        except IsADirectoryError:
            pass


#
get_page()

