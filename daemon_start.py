import sys, time
from daemon_class import daemon
import os


class MyDaemon(daemon):
    def run(self):
        while True:
            files = [r'/home/v-osipov/PycharmProjects/vba/data/malicious/' + file for file in
                     os.listdir(r'/home/v-osipov/PycharmProjects/vba/data/malicious/')]
            total = len(files)
            current_ben = len(os.listdir(r'/home/v-osipov/PycharmProjects/vba/data/benign_from_mal/'))
            current_mal = len(os.listdir(r'/home/v-osipov/PycharmProjects/vba/data/mal_from_mal/'))

            with open(r'/home/v-osipov/PycharmProjects/vba/data/log.vt', 'r') as f:
                last_strings = f.readlines()

            k = -1
            while last_strings[k].find('moved=') < 0:
                k -= 1

            last_string = last_strings[k]

            lg, rg = last_string.find('moved=')+6, last_string.find('%')

            last_moved = int(last_string[lg:rg])
            moved = current_ben + current_mal
            speed = (moved - last_moved) / 2

            with open(r'/home/v-osipov/PycharmProjects/vba/data/log.vt', 'a') as f:
                f.write(
                    f"total={total}  moved={moved} %={(moved / total) * 100} actual duration: {(time.time() - start) / 60 / 60} hours\n"
                    f"quantity mal={current_mal}  quantity ben={current_ben} speed={speed} files/min ~remaining time={total / speed} min \n")

            time.sleep(60 * 2)


if __name__ == "__main__":
    start = time.time()
    with open(r'/home/v-osipov/PycharmProjects/vba/data/log.vt', 'a') as f:
        f.write(f"start log at {time.ctime()}\n")

    daemon = MyDaemon('/tmp/loger.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print
            ("Unknown command")
            sys.exit(2)
        sys.exit(0)
    else:
        print
        ("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)
