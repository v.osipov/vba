import os
import pandas as pd
import random

path_benign = r'/home/v-osipov/PycharmProjects/vba/scrpts/new_scrpts/'
path_malicios = r'/home/v-osipov/PycharmProjects/vba/data/malicious/'

files_benign = list(map(lambda x: path_benign + x, os.listdir(path_benign)))

files_malicious = list(
    map(lambda x: path_malicios + x, random.sample(os.listdir(path_malicios)[:len(files_benign)], len(files_benign))))
#
data = pd.DataFrame()

for b, m in zip(files_benign, files_malicious):
    try:
        with open(b) as f:
            data = data.append({'file_name': b, 'text': f.read(), 'virus': 0}, ignore_index=True)

        with open(m) as f:
            data = data.append({'file_name': m, 'text': f.read(), 'virus': 1}, ignore_index=True)
    except UnicodeDecodeError:
        pass

data.to_csv('data2.csv')
