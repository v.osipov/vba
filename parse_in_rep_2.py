from bs4 import BeautifulSoup
import re
import requests
import os
import time
import sys
sys.setrecursionlimit(10000)

path_sc = os.getcwd() + '/vba_scrp_from_git/'


def write_scrp(url):
    url = url.split('https://github.com/')[1]
    url = 'https://raw.githubusercontent.com/' + ''.join(url.split('/blob'))
    req = requests.get(url)
    if req.status_code == 200:
        file_name = url.split('/')[-1]
        try:
            # while os.path.isfile(path_sc + file_name):
            #     temp_str = file_name.split('.')
            #     if temp_str[0][-1] == ')' and temp_str[0][-2].isdigit():
            #         temp_str[0] = temp_str[0][:-2] + str(int(temp_str[0][-2]) + 1) + ')'
            #         file_name = '.'.join(temp_str)
            #     else:
            #         temp_str[0] += "(1)"
            #         file_name = '.'.join(temp_str)
            with open(path_sc + file_name, 'w') as f:
                f.write(req.text)
                print('writed',file_name)
                return True
        except IsADirectoryError:
            pass
    else:
        time.sleep(2)
        write_scrp(url)


def read_url(url, url_buffer=[], exs=['.vba', '.cls', '.bas']):
    dirs = BeautifulSoup(requests.get(url).text, 'html.parser').find_all(
        class_='css-truncate css-truncate-target d-block width-fit')
    if dirs:
        url_buffer = get_refs(dirs) + url_buffer
        return read_url(url_buffer.pop(0), url_buffer)
    else:
        if url_buffer:
            if url[-4:] in exs:
                write_scrp(url)
            return read_url(url_buffer.pop(0), url_buffer)
        else:
            return True


def get_refs(obj):
    if obj:
        h_refs = [r'https://github.com' +
                  href.a.attrs['href'] for href in obj if href.a.attrs['href'].find('/commit/') == -1]
        return h_refs
    else:
        return False


def get_page():
    for n in range(20, 101):
        req = requests.get(f'https://github.com/search?l=Visual+Basic+.NET&p={n}&q=vba&type=Repositories')
        get_write_code(req.text)


#
#
def get_write_code(page_html):
    soup = BeautifulSoup(page_html, 'html.parser')
    divs = soup.find_all(class_='f4 text-normal')

    for element in divs:
        string = element.a.attrs['data-hydro-click']
        url = re.search(r'(https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?', string)
        repo_url = url.group(0)
        print(repo_url)
        read_url(repo_url)


#
get_page()
